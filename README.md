# README #

### Welcome ### 
Welcome to the Vector Prison repo.

### Features ###
-- Player Levelling (progression system)
-- Guards (w/ npc player tracking)
-- Gangs
-- Custom Scoreboard
-- Custom Economy (SQL)


### To Do ###
-- Merge all plugins to this one.
-- Skill Trees
-- Finish enchanting.


### Compilation Requirements ###
You will require:
-- Vector Core
-- WorldGuard
-- Vault
-- Protocol Lib
-- commons-configuration
-- commons-io
-- PermissionsEX